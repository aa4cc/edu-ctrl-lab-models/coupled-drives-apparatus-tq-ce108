# Coupled Drives Apparatus TQ CE108

Supporting data and codes for [Coupled Drives Apparatus CE108](https://www.tecquipment.com/coupled-drives-apparatus) from [TecQuipment](https://www.tecquipment.com/) located at our K26 lab.

The system contains two motorized wheels that are coupled through a flexible belt. The belt also passes around another (top) wheel whose axle is attached to an arm supported by a spring –⁠ a varying belt tension causes the arm to swing as the top wheel's axle moves vertically.

![TQ CE108](report/figures/ce108.png)

## Description of control inputs and measured outputs

### Control (also manipulated) inputs

| Name                           | Symbol   | Range    | Unit     |
|--------------------------------|----------|----------|----------|
|Voltage on the left DC motor    | u_m_1    | [-10,10] | V        |
|Voltage on the left DC motor    | u_m_2    | [-10,10] | V        |

### Measured outputs

| Name                                                         | Symbol   | Range    | Unit     |
|--------------------------------------------------------------|----------|----------|----------|
|Tacho voltage corresponding to the left motor angular rate    | u_omega_1| [-10,10] | V        |
|Tacho voltage corresponding to the right motor angular rate   | u_omega_2| [-10,10] | V        |
|Tacho voltage corresponding to the top wheel angular rate     | u_omega_p    | [-10,10] | V        |
|Potentiometer voltage corresponding to the (swing) angle of the arm carrying the top wheel axle | u_theta_p       | [-10,10] | V        |

## I/O data exchange between the CE108 model and a PC

The model is interconnected with a PC through a [data acquisition card MF614](https://www2.humusoft.cz/www/datacq/manuals/mf614um.pdf?_ga=2.222662365.712766263.1606940681-1700172487.1606940681) from [Humusoft](https://www.humusoft.cz/). PC runs MS Windows operating system and the latest version of Matlab and Simulink suite. In particular, [Simulink Desktop Real-Time](https://www.mathworks.com/products/simulink-desktop-real-time.html) library contains Simulink blocks for accessing the input and outputs on the MF614 card. 